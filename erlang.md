# Programming Erlang

[TOC]

## Erlang特性

- 函数式语言
- 并行编程语言

## 顺序编程

### Quick Start

#### Hello, World

```erlang
-module(hello).
-export([start/0]).

start() ->
  io:format("hello, world~n").
```

#### Fibonacci

```erlang
fibonacci(0) -> 1;
fibonacci(1) -> 1;
fibonacci(N) when N>0 -> fibonacci(N-1) + fibonacci(N-2);
```

#### File Server

```erlang
-module(afile_server)
-export([start/1, loop/1]).

start(Dir) -> spawn(afile_server, loop, [Dir]).

loop(Dir) ->
    receive
        {Client, list_dir} ->
	        Client ! {self(), file:list_dir(Dir)};
	    {Client, {get_file, File}} ->
	        Client ! {self, file:read_file(filename:join(Dir, File))}
    end,
    loop(Dir).
```

```erlang
-module(afile_client).
-export([ls/1, cat/2]).

ls(Server) ->
    Server ! {self(), list_dir},
	receive
	    {Server, FileList} ->
		    FileList
	end.

cat(Server, File) ->
    Server ! {self(), {get_file, File}},
	receive
	    {Server, Content} ->
		    Content
	end.
```

### 模式匹配操作符

```erlang
X = 10.
```

变量`X`绑定一个值`10`之后，就不能重新绑定变量。`=`不是赋值运算符，而是模式匹配操作符。

- 无副作用
- 并发
- 性能瓶颈

### 原子

原子以小写字母开头,后接一串字母、数字、下划线`_`或`@`符号，例如 `red, december,
cat, meters, yards, joe@somehost, a_long_name`。

### 元组

```erlang
Person = {person, {name, joe}, {height, 1.82}, {footsize, 42}, {eyecolor, brown}}.
```

从元组中提取字段值：

```erlang
Point = {point, 10, 20}.
{point, X, Y} = Point.
```

使用匿名变量`_`提取元组中的字段值：

```erlang
{_, {_, Name}, _, _, _} = Person.
```

### 列表

如果 `T` 是一个列表,那么 `[H|T]` 也是一个列表,它的头是 `H` ,尾是 `T` 。竖线( `|` )把列表的头与尾分隔开。 `[]` 是一个空列表。

列表元素可以为不同的类型。

```erlang
[{apples, 10}, {pears, 20}, {milk, 5}, {oranges, 5}].
[7, {apples, 10}, [10, apple, blue]].
```

如果`L`为非空的列表，则`[H|T] = L`，可以将列表的头提取到`H`中，尾提取到`T`中。

```erlang
Things = [{apples, 10}, {pears, 20}, {milk, 5}, {oranges, 5}].
[Thing1, Thing1 | RemainThings] = Things.
```

### 字符串

Erlang的字符串实际上是一个整数列表，字符串仅仅是其速记形式而已。

```erlang
"hello, world".
[$s-32, $u, $r, $p, $r, $i, $s, $e].
```

### 模块

#### geometry

```erlang
-module(geometry).
-export([area/1]).

area({rectangle, Widith, Height}) -> Width * Height;
area({circle, Radius}) -> 3.14 * Radius * Radius;
area({square, Side}) -> Side * Side.
```

通过如下的方式调用`genometry:area`

```erlang
genometry:area({rectangle, 10, 20}).
```

#### shop

```erlang
-module(shop).
-export([total/1]).

cost(oranges)   -> 5;
cost(apples)    -> 10;
cost(pears)     -> 8;
cost(milk)      -> 5;
cost(newspaper) -> 6.

total([{What, N}|T]) -> cost(What) * N + total(T).
total([])            -> 0;
```

#### 同名不同元函数

```erlang
sum(L) -> sum(L, 0).

sum([], N)    -> N;
sum([H|T], N) -> sum(T, H + N);
```

事实上，可以`sum`实现可简化为：

```erlang
sum([])    -> 0;
sum([H|T]) -> H + sum(T).
```

### 匿名函数fun

```erlang
L = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
lists:map(fun(X) -> 2*X end, L).
```

可以为`fun`绑定一个变量。

```erlang
Even = fun(X) -> (x rem 2) =:= 0.
lists:filter(Even, L).
```

`func`可以有多个子句，或多个参数。

```erlang
Temperature = fun({c, C}) -> {f, 32 + C*9/5};
                 ({f, F}) -> {c, (F-32)*5/9}
              end.
```

fun也可以作为返回值。

```erlang
Multi = fun(Times) -> (fun(X) -> X*Times end) end.

Double = Multi(2).
Tripe = Multi(3).

Double(10).
Tripe(10).
```

### 自定义控制结构

```erlang
for(Max, Max, F) -> [F(Max)];
for(I, Max, F)   -> [F(I) | for(I+1, Max, F)].
```

可以给for传递任意的fun。

```erlang
for(0, 9, fun(X) -> X*X end).
```

等价于：

```erlang
lists：map(fun(X) -> X*X end, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).
```

事实上，诸如`lists:map, for`等，能够返回或者以`fun`为参数的函数称为高阶函数。


#### sum

```erlang
sum([])    -> 0;
sum([H|T]) -> H + sum(T).
```

#### map

```erlang
map(_, [])    -> [];
map(F, [H|T]) -> [F(H), map(F,T)].
```

#### 重构shop

```erlang
-module(shop).
-export([total/1]).
-import(lists, [sum/1, map/2]).

cost(oranges)   -> 5;
cost(apples)    -> 10;
cost(pears)     -> 8;
cost(milk)      -> 5;
cost(newspaper) -> 6.

total(L) -> sum(map(fun({What, N} -> cost(What)*N end, L)).
```

### 列表推导

`[F(X) || X <- L]`推导为：由`F(X)`组成的列表(`X`从列表`L`中提取)。

```erlang
L = [1, 2, 3, 4].
lists:map(fun(X) -> 2*X end, L).

[2*X || X <- L].
```

也就是说`map`的实现可以简化为：

```erlang
map(F, L) -> [F(X) || X <- L].
```

一般地，`[X || Qualifier1, Qualifier2, ...]`， 其中`Qualifiern`可以是生成器，位串生成器，或者过滤器。

```erlang
[X || {a, X} <- [{a, 1}, {b, 2}, {a, 3}, hello, "world"]].
```

#### 重构shop

```erlang
-module(shop).
-export([total/1]).

cost(oranges)   -> 5;
cost(apples)    -> 10;
cost(pears)     -> 8;
cost(milk)      -> 5;
cost(newspaper) -> 6.

total(L) -> lists:sum([cost(What)*N || {What, N} <- L])).
```

#### Quicksort

```erlang
qsort([Pivot|T]) ->
    qsort([X || X <- T, X < Pivot])
	++ [Pivot] ++
    qsort([X || X <- T, X >= Pivot]);
qsort([]) -> [].
```

其中, `++`为列表的连接符。

#### 毕达哥拉斯三元数组

```erlang
pythag(N) ->
    [ {A, B, C} ||
	  A <- lists:seq(1, N),
	  B <- lists:seq(1, N),
	  C <- lists:seq(1, N),
	  A+B+C =< N,
	  A*A + B*B =:= C*C
	].
```

#### 回文构词

```erlang
perms([L]) -> [[H|T] || H <- L, T <- perms(L -- [H])].
perms([])  -> [[]];
```

### guard

```erlang
max(X, Y) when X > Y -> X;
max(X, Y) -> Y.
```

### case

#### filter

```erlang
filter(F, [])    -> [];
filter(F, [H|T]) ->
    case F(H) of
	    true  -> [H|filter(F, T)];
		false -> filter(F, T)
	end.
```

#### 累加器

```erlang
odds_and_evens(L) ->
    Odds  = [X || X <- L, (X rem 2) =:= 1],
    Evens = [X || X <- L, (X rem 2) =:= 0],
    {Odds, Evens}.
```

上述实现需要遍历两次列表，可以采用如下的实现，遍历一次即可。

```erlang
odds_and_evens(L) ->
    odds_and_evens(L, [], []).

odds_and_evens([H|T], Odds, Evens) ->
    case (H rem 2) of
        0 -> odds_and_evens(T, Odds, [H|Evens]);
        1 -> odds_and_evens(T, [H|Odds], Evens)
    end;
odds_and_evens([], Odds, Evens) ->
    {lists:reverse(Odds), lists:reverse(Evens)}.
```

## 记录

```erlang
-record(todo, {status=reminder, who=joe, text}).
```

```erlang
#todo{}.
X1 = #todo{status=urgent, text="fix bugs"}.
X2 = X1#todo{status=done}.
```

通过如下的方式提取字段的值。

```erlang
#todo{who=Who, text=Text} = X1.
```

或者：

```erlang
X2#todo.who.
```

函数中记录的匹配常常存在如下形式：

```erlang
clean_status(#todo{status=Status, text=Text} = R) ->
    R#todo{status=finished}.
```

```erlang
do_something(X) when is_record(X, todo) ->
    ...
```

### 异常


### BIF

```erlang
tuple_to_list/1
time/0
```

### 惰性求值

### 可重入的解析器

### 解析组合子

### Design

- Solving Problem with Design
- Not with the implemention language construction.
- What's the Design? 问题的可计算
- Good Desing? 建立匹配的语义模型
- What's the Computation?
- 软件工程：控制软件的复杂度

### A History of Erlang

#### Fault-tolerant

Crash-Oriented

- Isoltation.
- Copying.
- Share nothing.
- Enough local information to carry on running..

- Reactive Programming.

控制面的本质：

- 性能
- 控制的复杂度

分布式系统设计



### TDD

#### 物理结构

```bash
coderetreat
├── ebin
├── rebar
├── src
│   ├── coderetreat.app.src
│   └── coderetreat.erl
└── test
    └── coderetreat_test.erl
```

执行如下命令来运行测试用例：

```bash
$ ./rebar eunit
```

```erlang
-module(coderetreat_test).
-compile(export_all).

-import(coderetreat, [something/0]).

-include_lib("eunit/include/eunit.hrl").

something_test() ->
    ?assert(1 =:= something()).
```