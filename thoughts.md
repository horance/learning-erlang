# Thoughts of Learning Erlang

[TOC]

## 软件设计的再次思考

### 程序设计语言设计的哲学原理

程序设计语言虽然只是程序员的一件工具，但它是程序员与软件沟通的唯一的桥梁，也是程序员设计思维体现到软件实体中唯一的渠道，更是程序员之间最直接、最权威的沟通媒介。

无论是那一种程序设计语言，往往都存在各种各样的模式、习惯用法、或者最佳实践。我在学习一门新的语言时，首先就是收集社区中优秀的实践，通过这些模式、习惯用法、最佳实践的收集，让你在使用语言做设计时不会犯下重复的错误，这让我偏离方向的概率极具降低。

但这远远不够，我们需要通过长时间的学习、实践、总结、思考，才能形成自身的知识体系，并形成属于自己的哲学逻辑。

当然，这个过程往往充满着各种不确定性，只有了解业界、社区大师的优秀思考，方能促进自己思维的升华，这非常重要。

### 我需要精通所有语法吗？

对于这个问题往往存在两种极端，一种人认为只需要知道`if, while, for`便能行天下了。另外一种人喜欢死扣语言的死角。

例如更多的人在学习C\+\+语言时，都在纠结在C\+\+的语法细节上，他们不是在使用C\+\+做设计，而是在卖弄各种奇技淫巧，让设计变得更加错综复杂。

事实上，除了语言缔造者之外，没有任何一个人能够完全精通一门语言。实际项目实践过程中，我们只会用到语言中基本的特性。对于死角的语言特性，大可不用理睬，因为那些往往是语言设计时存在的缺陷而已。

### 灵活性能给设计带来好处吗？

灵活性往往是一把双刃剑，高手很喜欢语言的灵活性，而菜鸟在面对各种选择时往往会迷失方向。对于软件设计本身，我认为语言的灵活性、可扩展性是很重要的。其次语言本身一定要很自由，不能束缚程序员的思想。

### 混合范型还是单一范型？

很多种语言都存在多种编程范型供程序员选择，如果单纯的面向对象往往使人不够自由，因为在一些场景下，不得不为面向对象而面向对象，不够直接，反而拖泥带水。

我更喜欢混合的范型，根据不同场景，不同的上下文，使用不同范型，解决不同的问题。

### 动态类型还是静态类型

静态语言的两个缺点常常被人诟病：

- 冗长的语法
- 有限的错误检查

```java
Map<int, String> dict = new HashMap<int, String>();
```

这样的重复让人很痛苦，难道右边的类型，还不够明确吗？事实上，我们更期望的是这样的语法。

```scala
dict = new HashMap<int, String>();
```

这样冗长的语法，随着现代编译技术的发展，现状得到了改善。C++11, Scala在这方面取得了很好的成绩，但Java表现则不是那么理想。例如在C\+\+11中，`auto i = std::make_shared<int>(5)`语句中，编译器能够自动推演类型。

至于错误检查，动态语言的支持者说，他们能够使用UT保证错误不泄漏，或者发誓错误不会发生。但这样的观点我不认同，因为UT只能证明错误的存在，而不能证明错误不存在。

当然动态语言也存在若干方面的优点：

- 简单、直接、自由
- 运行时的计算，完成诸如在线修复等很酷的功能

但是，动态语言往往使得重构变得及其危险，而使得测试覆盖变得更加必不可少，但实际情况是，项目中总是存在缺失的用例，运行时崩溃是动态语言程序员永远的一个噩梦。

### 需要学习多种程序设计语言吗？

Dave Thomas在书中鼓励大家尝试多种语言，这是一件快乐的事情。但世界上语言万千，有好有坏，这需要我们能够学会取舍。

我认为不要盲目地准求数量，而是尝试不同范型的实践。例如命令式用惯了，尝试一下函数式。结构化用惯了，可以尝试一下面向对象，或者是逻辑式。

多种思维的思考，往往会给你带来不同的认识，从而帮助你完成更好的设计。

### OO和FP是矛盾的吗？

两者并不矛盾，虽然两个阵营从一开始就互相对骂直至今日，上到大师，下到屌丝。事实上，两个社区存在优秀的思考方式，并创造了伟大的作品，甚至像Scala提出了将FP和OO结合在一起的想法，也是一种勇敢的尝试。

### 什么是好的设计?

邓辉老师对于这个问题给出了一个新的思路，让我耳目一新。建立一个简单的、直接的语义模型，往往需要丰富的软件设计经验和对问题本质的理解和抽象。所以要做好软件设计，必须通过不断的实践和思考方能做到。

### 设计模式的本质是什么？

项目中很多OO设计者往往会在这个方面犯错，他们喜欢使用模式来解决世界上所有的问题，甚至是套用设计模式，这就形成了滥用，是一种有害的实践。

事实上，设计模式只是前人留下来的一些宝贵经验，它们都有一个共同的本质：依赖管理。依赖管理是OO设计最关键，也是最困难的技术。只有通过遵循良好的设计原则，才能保证适宜的、低耦合的依赖管理。

为此设计模式应该是一个很自然的过程，如果能够保证良好的设计原则，我们大可忘记所有的设计模式。

### 我的最爱

我熟悉多种程序设计语言，例如C\+\+, Java, Ruby, Scala，到目前为止C\+\+, Scala是我的最爱，因为它们都有一个共同点：支持多种范式。

也许很多人批评C\+\+，但它的确有很多的优点。尤其C\+\+11，拥有强大的类型推演能力，让其诟病的静态编译时冗长的语法得到了彻底的改观，让你发现你可以像Ruby，Python一样来使用C\+\+11，简单而富有表达力。

我最爱C\+\+是因为其具有让你很大的选择的空间，而不是让你局限在一个很小的空间里而不能呼吸。工具越丰富，往往给我的设计带来了更多的灵活性。

当然了，一定要清楚地认识到：威力越大，责任就越大。我们使用C\+\+是为了做更好的设计，而不是卖弄各种技巧，这就本末倒置了。

### 第一次亲密接触Erlang

也在这个周我开始阅读两位Erlang设计大师的作品：《Programing Erlang》和《Erlang Programming》，也体会到了函数式设计、及其并发程序设计的逻辑和思考方式，但我接触Erlang的时间很短，也没有深刻理解其内在的哲学原理，希望通过后面的实践，增强自我的认识。

## Fizz-Buzz-Whizz

### 语义模型

对问题本身建立合理的、抽象的语义模型是软件设计的根本所在，有了统一的语义模型，那采用是FP的，还是OO，差别仅仅是在于谁更直接和自然的问题了。

对于面向对象，如果设计经验不足，往往引入过于复杂的依赖和委托，掩盖了问题的本质；而FP一直在工业界一直没有成为主流，更多地还是因为大部分程序员不习惯像数学家一样的思维方式思考问题的本质。

在面对Fizz-Buzz-Whizz这个问题时，我刚开始彻底蒙了，因为我无从下手。问题域究竟是什么？我们该如何描述我们的问题？

事实上，虽然问题的输入和输出很明确，但这个问题本质却很隐晦。例如规则与规则之间的关联关系，是需要抽象和深入地思考的，经过整理后的语义模型形式化后可能是这样的：

```ruby
Spec ::= AND(Rule(1), Rule(2), ..., Rule(n) | OR(Rule(1), Rule(2), ..., Rule(n)
Rule ::= Rule | Atom
Atom ::= (Matcher, Action) -> bool
Matcher ::= (int) -> bool
Action  ::= (int) -> string
```

在邓辉老师的指导下，我慢慢地发现了问题的本质语义模型，将上面的形式化的、抽象的逻辑再明细化一次。

```ruby
r1_3 = atom(times(3), to("Fizz"));
r1_5 = atom(times(5), to("Buzz"));
r1_7 = atom(times(7), to("Whizz"));

r1 = anyof(r1_3, r1_5, r1_7);

r2 = anyof( allof(r1_3, r1_5, r1_7)
          , allof(r1_3, r1_5)
          , allof(r1_3, r1_7)
          , allof(r1_5, r1_7));

r3 = atom(contains(3), to("Fizz"));
rd = atom(always(true), nop());

spec = anyof(r3, r2, r1, rd);
```

有了这个语义模型之后，接下来就是用一门程序设计语言来描述它就可以了。

### Java的实现

#### Java实现的DSL

你会发现DSL是一个很自然的过程，它往往是设计语义模型时的一个副产品。这不是偶然，因为问题本质的语义模型往往相对稳定，有了正确语义模型的抽象，DSL的出现绝对是必然的。

```java
private static Rule makeSpec() {
    Rule r1_3 = atom(times(3), to("Fizz"));
    Rule r1_5 = atom(times(5), to("Buzz"));
    Rule r1_7 = atom(times(7), to("Whizz"));

    Rule r1 = anyof(r1_3, r1_5, r1_7);

    Rule r2 = anyof( allof(r1_3, r1_5, r1_7)
                   , allof(r1_3, r1_5)
                   , allof(r1_3, r1_7)
                   , allof(r1_5, r1_7));

    Rule r3 = atom(contains(3), to("Fizz"));
    Rule rd = atom(always(true), nop());

    return anyof(r3, r2, r1, rd);
}
```

#### Matcher的OO设计

`Matcher`其实是`int -> bool`的一个映射关系。

```ruby
matcher ::= (int) -> bool
```

如何设计Matcher呢，因为Java倡导"一切皆是OO"的哲学思维，我们所能选择的就是为它们建立共同的抽象。在这里，我简单使用了如下的几个关键设计：

- 对于Matcher统一的接口抽象
- 静态工厂方法，实现差异性的定制
- 没有定义`times_3`, `times_5`等，而是定义更为抽象的、本质的`times`; always也是一样的，如果需求要求，你也可以使用`always(false)`
- 匿名的内部类，实现假象的“闭包”；这样`times`的参数被可以直接在内部类中使用。

```java
public interface Matcher {
    boolean matches(int n);

    static Matcher times(int times) {
        return new Matcher() {
            @Override
            public boolean matches(int n) {
                return n % times == 0;
            }
        };
    }

    static Matcher contains(int num) {
        return new Matcher() {
            @Override
            public boolean matches(int n) {
                int m = 0;
                while (n>0) {
                    m = n%10;
                    n = n/10;
                    if (m == num) return true;
                }
                return false;
            }
        };
    }

    static Matcher always(boolean logic) {
        return new Matcher() {
            @Override
            public boolean matches(int n) {
                return logic;
            }
        };
    }
}
```

#### Action的OO设计

Action其实就是将`int`变换为`String`的一个抽象。

```ruby
Action ::= int -> String
```

Action的设计思路与Matcher的设计思路基本一致，有几个思考点。

- `to_fizz, to_buzz, to_whizz`是典型的结构重复，其本质应该是`to(String) -> String`
- `nop`犹如一个`Null Object`，给设计带来了完整性和统一性

```java
public interface Action {
    String to(int n);

    static Action to(String str) {
        return new Action() {
            @Override
            public String to(int n) {
                return str;
            }
        };
    }

    static Action nop() {
        return new Action() {
            @Override
            public String to(int n) {
                return Integer.toString(n);
            }
        };
    }
}
```

#### Rule的OO设计

`Rule`的设计体现在如下几个方面：

- 消除`anyof, allof`的重复逻辑，为次提出了`CombinableRule`的共同抽象
- `apply`有义地设计为`boolean`类型，让`CombinableRule`的实现更加自然
- `RuleResult`作为`Collection Parameter`，提升效率的同时也得到了更好的设计

```java
public interface Rule {
    boolean apply(int n, RuleResult rr);

    static Rule atom(Matcher matcher, Action action) {
        return new Rule() {
            @Override
            public boolean apply(int n, RuleResult rr) {
                return rr.collect(matcher.matches(n), action.to(n));
            }
        };
    }

    static class CombinableRule implements Rule {
        CombinableRule(boolean shortcut, Rule...rules) {
            this.shortcut = shortcut;
            this.rules = Arrays.asList(rules);
        }

        @Override
        public boolean apply(int n, RuleResult rr) {
            for (Rule r : rules) {
                if (r.apply(n, rr) == shortcut) {
                    return shortcut;
                }
            }
            return !shortcut;
        }

        private boolean shortcut;
        private List<Rule> rules;
    }

    static Rule anyof(Rule...rules) {
        return new CombinableRule(true, rules);
    }

    static Rule allof(Rule...rules) {
        return new CombinableRule(false, rules) {
            @Override
            public boolean apply(int n, RuleResult rr) {
                RuleResult result = new RuleResult();
                return rr.collect(super.apply(n, result), result);
            }
        };
    }
}
```

#### Java实现的几个遗憾之处

- 建立统一的、抽象的接口，是经典的OO设计哲学，但有时候往往不够直接、自然
- OO设计需要更多的精力花在类与类之间的依赖管理的问题上
- 缺失更为本质的`combs`的抽象，但使用OO显得有点困难

```java
private static Rule makeSpec() {
    ...
    Rule r1 = anyof(r1_3, r1_5, r1_7);

    Rule r2 = anyof( allof(r1_3, r1_5, r1_7)
                   , allof(r1_3, r1_5)
                   , allof(r1_3, r1_7)
                   , allof(r1_5, r1_7));
    ...

```

其本质应该由`{r1_3, r1_5, r1_7}`便能推出`{r1, r2}`的，但使用`OO`的设计思路，相对FP难度较大。

### C++的实现

#### C\+\+的DSL设计

C\+\+依然可以得到很漂亮的DSL，实现语义模型时也很方便和直接。

```cpp
FIXTURE(RuleTest)
{
    RuleResult result;
    SharedRule spec = make_spec();

    SharedRule make_spec()
    {
        auto r1_3 = atom(times(3), to("Fizz"));
        auto r1_5 = atom(times(5), to("Buzz"));
        auto r1_7 = atom(times(7), to("Whizz"));

        auto r1 = anyof({r1_3, r1_5, r1_7});

        auto r2 = anyof({ allof({r1_3, r1_5, r1_7})
                        , allof({r1_3, r1_5})
                        , allof({r1_3, r1_7})
                        , allof({r1_5, r1_7})
                        });

        auto r3 = atom(contains(3),  to("Fizz"));
        auto rd = atom(always(true), nop());

        return anyof({r3, r2, r1, rd});
    }

    void rule(int n, const std::string& expect)
    {
        spec->apply(n, result);
        ASSERT_THAT(result, eq(RuleResult(expect)));
    }

    TEST("r1_3")
    {
        rule(3, "Fizz");
    }

	TEST("r1_5")
    {
        rule(5, "Buzz");
    }

    TEST("r1_7")
    {
        rule(7, "Whizz");
    }

    TEST("r3")
    {
        rule(13, "Fizz");
    }

    TEST("r2_1")
    {
        rule(3*5*7, "FizzBuzzWhizz");
    }

    TEST("r2_2")
    {
        rule(3*5, "FizzBuzz");
    }

    TEST("r2_3")
    {
        rule(3*7, "FizzWhizz");
    }

    TEST("r2_4")
    {
        rule((5*7)*2, "BuzzWhizz");
    }

    TEST("priority of r3 greater than r2_4")
    {
        rule(5*7/*35*/, "Fizz");
    }

    TEST("rd")
    {
        rule(2, "2");
    }
};
```

#### C\+\+的内存管理

使用`std::shared_ptr`，其内存管理相对于C语言来说，简单、好用。

```cpp
#ifndef HAFB02649_F7FE_4C8D_BF52_F3B241A9B753
#define HAFB02649_F7FE_4C8D_BF52_F3B241A9B753

#include "game/Action.h"
#include "game/Matcher.h"
#include <vector>

struct RuleResult;

DEFINE_ROLE(Rule)
{
    ABSTRACT(bool apply(int n, RuleResult& rr) const);
};

typedef std::shared_ptr<Rule> SharedRule;

SharedRule atom(const SharedMatcher&, const SharedAction&);
SharedRule anyof(const std::vector<SharedRule>&);
SharedRule allof(const std::vector<SharedRule>&);

#endif
```

#### 遗憾：C\+\+闭包支持

相对于FP，甚至时Java实现时，C\+\+在捕获上下文稍逊了一些（当然存在其的设计方法能够做到，再次仅仅探讨我目前的设计），你不得不建立相应的构造函数，并持有它们。

```cpp
#include "game/Rule.h"
#include "game/RuleResult.h"

namespace
{
    struct Atom : Rule
    {
        Atom(const SharedMatcher& matcher, const SharedAction& action)
          : matcher(matcher), action(action)
        {}

    private:
        OVERRIDE(bool apply(int n, RuleResult& rr) const)
        {
            return rr.collect(matcher->matches(n), action->to(n));
        }

    private:
        SharedMatcher matcher;
        SharedAction  action;
    };

    struct CombinableRule : Rule
    {
        CombinableRule(bool shortcut, const std::vector<SharedRule>& rules)
          : shortcut(shortcut), rules(rules)
        {}

    protected:
        OVERRIDE(bool apply(int n, RuleResult& rr) const)
        {
            for(auto rule : rules)
            {
                if(rule->apply(n, rr) == shortcut)
                    return shortcut;
            }
            return !shortcut;
        }

    private:
        bool shortcut;
        std::vector<SharedRule> rules;
    };

    struct Anyof : CombinableRule
    {
        Anyof(const std::vector<SharedRule>& rules)
          : CombinableRule(true, rules)
        {}
    };

    struct Allof : CombinableRule
    {
        Allof(const std::vector<SharedRule>& rules)
          : CombinableRule(false, rules)
        {}

    private:
        OVERRIDE(bool apply(int n, RuleResult& rr) const)
        {
            RuleResult result;
            return rr.collect(CombinableRule::apply(n, result), result);
        }
    };
}

SharedRule atom(const SharedMatcher& matcher, const SharedAction& action)
{ return std::make_shared<Atom>(matcher, action); }

SharedRule anyof(const std::vector<SharedRule>& rules)
{ return std::make_shared<Anyof>(rules); }

SharedRule allof(const std::vector<SharedRule>& rules)
{ return std::make_shared<Allof>(rules); }
```

## 对Erlang的感悟

### 一切皆function

FP允许入参传递function，和返回一个function，这个给设计带来了巨大的灵活性。一切皆function的哲学思维，给了一直以来信奉OO的我很大的震撼。

神奇的FP操作，将两个参数变成了一个参数的操作，让FP的编程充满了乐趣。例如函数式的`times`完胜Java的实现，简单易懂，直接与问题域一一映射。

```erlang
times(N) ->
    fun(X) -> (X rem N) =:= 0 end.
```

### 递归的思维

短短三句就能实现一个快速排序，关键它能与算法一一对应，简直不可思议。

```erlang
qsort([Pivot|T]) ->
    qsort([X || X <- T, X < Pivot])
    ++ [Pivot] ++
    qsort([X || X <- T, X >= Pivot]);
qsort([]) -> [].
```

再看一下`map`的实现，漂亮的递归思维：

```erlang
map(F, []) -> [];
map(F, [H|T]) -> [F(H) | map(F, T)].
```

当然，还可以更简单、直接：

```erlang
map(F, L) -> [F(X) || X <- L].
```

### 模式匹配

类似与Prolog的模式规则，让Erlang的函数实现更加简单、漂亮。例如Programing Erlang书中的File Server的例子，漂亮的“Selective Receive”模式相当漂亮、简单。

```erlang
-module(afile_server)
-export([start/1, loop/1]).

start(Dir) -> spawn(afile_server, loop, [Dir]).

loop(Dir) ->
    receive
        {Client, list_dir} ->
	        Client ! {self(), file:list_dir(Dir)};
	    {Client, {get_file, File}} ->
	        Client ! {self, file:read_file(filename:join(Dir, File))}
    end,
    loop(Dir).
```

```erlang
-module(afile_client).
-export([ls/1, cat/2]).

ls(Server) ->
    Server ! {self(), list_dir},
	receive
	    {Server, FileList} ->
		    FileList
	end.

cat(Server, File) ->
    Server ! {self(), {get_file, File}},
	receive
	    {Server, Content} ->
		    Content
	end.
```

### 并发的思维

世界是并发的，Erlang的COP设计范型给软件架构带来的思考值得我们深思，尤其是摩尔定律实效，多核的时代的到临。

### 容错

Erlang为容错技术很值得我们深思，尤其对于长时间运行，不可停机的系统来说尤为重要。在这方面，Erlang在设计初始阶段将容错的设计思维深深地植入到了语言之中，给系统的架构带来深远的影响。

### 反思

在第一次实现`AND`的语义时，我几乎撕破头皮也没有做出来，一则是因为缺乏FP的思维习惯，二则FP的思维的确要难一点，需要一段时间思考和磨合。

```erlang
'AND'(Rules) ->
    'AND'(Rules, fun(A1, A2) -> A1 ++ A2 end, "").

'AND'([], _, Acc) -> fun(_) -> {true, Acc} end;
'AND'([H|T], Op, Acc) ->
    fun(N) ->
        case H(N) of
            {true, A} -> ('AND'(T, Op, Op(A, Acc)))(N); 
            false -> false
        end
    end.
```

## 期待

- 并发设计的架构
- 更多的FP思维模式
- 更多的容错设计思路
- 更深刻的OO思维的体会

## 附录

文中引用的代码可以在Gitlab上找到：[https://gitlab.com/horance/fizz-buzz-whizz](https://gitlab.com/horance/fizz-buzz-whizz)

## 致谢

首先感谢袁英杰先生，我曾在2011年南京举办的OO训练营课程中，第一次认识了袁英杰老师，虽然只是两天简短的培训，但彻底地改变了我对整个软件设计的认识，并使我建立了正确的价值导向和软件设计的知识体系，这也是我人生中最重要的一次转折点。

另外，感谢邓辉，孙鸣老师，曾在无线院第一次技术巡讲的时候，我第一次认识邓辉，孙鸣老师。邓辉不惜繁忙的工作时间给我教诲了很多软件设计的思想。时隔2年，我再次有幸参加邓辉，孙鸣老师的培训，您的思维方式、人生哲理，及其对软件设计本质的理解，让我再次耳目一新，感谢您给我们非同一般的感受。
